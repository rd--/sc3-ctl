import Sound.Osc {- hosc -}

-- | Requires sclang be at the standard port.
with_sclang :: Connection Udp a -> IO a
with_sclang = withTransport (openUdp "127.0.0.1" 57120)

send_message :: Message -> IO ()
send_message = with_sclang . sendMessage

-- > rctl_std "/home/rohan/sw/sc3-ctl/help"
rctl_std :: String -> IO ()
rctl_std = send_message . message "/rctl-std" . return . string

-- > rtbl_std "/home/rohan/sw/sc3-ctl/help"
rtbl_std :: String -> IO ()
rtbl_std = send_message . message "/rtbl-std" . return . string
