/* Initialise scsynth if required */
boot(s)

/* Initialise MIDI if required */
midiInit(RGui)

/* Observe OSC traffic */
dumpOSC(s, true)

/* The set of default options */
defaultOpt(RCtlSet)

/* RCtlSet options */
o = (rows: 3, columns: 8, use_slider: true);

/* Create a new RCtlSet */
c = RCtlSet(fd: s.addr, opt: o)

/* Setup the first RCtl (name, spec, value, bus_id, synth_id, synth_param, onEdit) */
setup(at(c, 0), "Freq", ControlSpec(220, 440, ExponentialWarp, 0, 330), nil, 0);
asDictionary(at(c, 0))

/* Store a function to be called whenever the value is edited */
onEdit_(at(c, 0), { arg ctl; postln(value(ctl)) })
onEdit_(at(c, 0), { arg ctl; basicColor_(ctl, if(value(ctl) > 220) { red(Color) } { background(RGui) }) })

/* The RCtl value can be updated from the server */
sendMsg(s, "/c_set", 0, 345)
update(at(c, 0))
value(at(c, 0))

/* Remove printer | coloriser */
onEdit_(at(c, 0), nil)

/* Make two Sine oscillators */
(
setup(c[0], "Freq-1", ControlSpec(220, 240, ExponentialWarp), 221, 0);
setup(c[1], "Gain-1", ControlSpec(0, 1/5, LinearWarp), 1/7, 1);
setup(c[8], "Freq-2", ControlSpec(220, 240, ExponentialWarp), 229, 8);
setup(c[9], "Gain-2", ControlSpec(0, 1/5, LinearWarp), 1/9, 9);
play({ ar(SinOsc, kr(In, [0, 8]), 0) * kr(In, [1, 9]) })
)

stop(thisProcess)

/* Variant that maps control inputs to control buses */
{
	var gr = { arg freq=700, amp=0.1; ar(Out, 0, ar(SinOsc, freq, 0) * amp) };
	var sy = SynthDef("sin", gr);
	add(sy);
	sync(s); /* add runs d_recv which is async */
	map(Synth("sin"), "freq", 0, "amp", 1);
	map(Synth("sin"), "freq", 8, "amp", 9)
}.fork

stop(thisProcess)

/* Variant that send n_set message to control nodes */
(
var n = [Synth("sin"), Synth("sin")];
var f = {
	arg k;
	setup(c[(k*8)], "Frequency", ControlSpec(220, 240, ExponentialWarp), 220, nil, nodeID(n[k]), "freq");
	setup(c[(k*8)+1], "Gain", ControlSpec(0, 0.5, LinearWarp), 0.1, nil, n[k].nodeID, "amp")
};
do([0, 1], f)
)

stop(thisProcess)

/* Use RCtl values at the language, here for scheduling nodes (pings) */
(
var gr = {
	arg out = 0, freq = 440, pan = 0, gain = 1;
	var e = kr(EnvGen, envelope: perc(Env), levelScale: gain, doneAction: 2);
	var o = ar(SinOsc, freq, 0);
	ar(Out, out, ar(Pan2, o, pan, e))
};
var sy = SynthDef("ping", gr);
var rt = {
	var scl = [
		[0, 2, 4, 5, 7, 9, 11],
		[0, 2, 3, 5, 6, 8, 9, 11],
		[0, 2, 4, 6, 8, 10]
	];
	var fn = {
		var freq = midicps(choose(scl[value(c[4])]) + 48 + choose([-12, 0, 12]));
		sendMsg(s, "/s_new", "ping", -1, 0, 0
      , "freq", freq * value(c[1])
      , "pan", value(c[2])
      , "gain", value(c[3]) * rand(0.25));
		yield(value(c[0]))
	};
	do(inf, fn)
};
send(sy, s);
setup(c[0], "Delay", ControlSpec(0.05, 0.75, LinearWarp), 0.18, 0);
setup(c[1], "Freq-Mul", ControlSpec(0.5, 2, LinearWarp), 1.75, 1);
setup(c[2], "Pan", ControlSpec(-1, 1, LinearWarp), 0, 2);
setup(c[3], "Gain", ControlSpec(0, 1, LinearWarp), 0.1, 3);
setup(c[4], "Scale", ControlSpec(0, 2, LinearWarp, 1), 0.0, 4);
play(Routine(rt), SystemClock);
)

/* Add a language level oscillator to the pan RCtl. This runs at the
  same rate as the node instantiation routine, though it is not
  synchronized. Note that the interface reflects changes at the
  RCtl. */
play(Routine { do(inf) { value_(c[2], rand(2.0) - 1.0); yield(value(c[0])) } })

stop(thisProcess)

/* Setup midi NoteOn messages to set RCtl values.
  A single sawtooth oscillator with NoteOn and NoteOff messages setting C values */
(
var prv = nil; /* most recent midi note number */
setup(c[16], "Note", ControlSpec(0, 127, LinearWarp, 1), 60, 16);
setup(c[17], "Gain", ControlSpec(0, 1, LinearWarp), 0, 17);
noteOn_(MIDIIn, {
	arg src, chan, mnn, vel;
	value_(c[16], mnn);
	value_(c[17], vel / 127.0);
	prv = mnn
});
noteOff_(MIDIIn, {
	arg src, chan, mnn, vel;
	if(mnn == prv, {value_(c[17], 0.0)})
});
play({ar(LFSaw, midicps(kr(In, 16)), 0) * kr(In, 17) * 0.1})
)

stop(thisProcess)

/* Two comb filters, one tuned using a delay time, the other a note input */
(
var gr = {
	var i = ar(SoundIn, 0); // ar(Decay2, ar(Dust, 1)) * 0.1 // ar(SoundIn, 0)
	var dt1 = kr(LagIn, 0) * 0.0001;
	var f1 = ar(CombC, i, 0.2, dt1, kr(LagIn, 1)) * kr(LagIn, 2);
	var dt2 = 1.0 / midicps(floor(kr(In, 3)));
	var f2 = ar(CombC, i, 0.2, dt2, kr(In, 4)) * kr(LagIn, 5);
	[f1, f2]
};
setup(c[0], "DelayTime-1", ControlSpec(1, 180, LinearWarp), 10, 0);
setup(c[1], "DecayTime-1", ControlSpec(0.01, 9.0, LinearWarp), 1, 1);
setup(c[2], "Gain-1", ControlSpec(0, 0.2, LinearWarp), 0.1, 2);
setup(c[3], "Note-2", ControlSpec(0, 127, LinearWarp, 1), 60, 3);
setup(c[4], "DecayTime-2", ControlSpec(0.01, 9.0, LinearWarp), 1, 4);
setup(c[5], "Gain-2", ControlSpec(0, 0.2, LinearWarp), 0.1, 5);
play(gr)
)

stop(thisProcess)

/* Name according to index */
(
close(window(c));
c = RCtlSet(fd: s.addr, opt: (rows:6, columns:6));
do(36, {arg i; setup(c[i], "Ctl:" ++ asString(i), \unipolar, 0.5, i)});
inspect(c[0])
)

/* RCtlSet implements the copySeries method, allowing expressions like: */
sum(collect(c[10..13], value(_)))

/* There is a snapshot mechanism that collects the current values */
~preset = snapshot(c, 0, 15)

/* Scramble... */
do(c[0 .. 15]) { arg ctl; value_(ctl, rand(1.0)) }

/* Restore values from snapshot. */
restore(c, ~preset)

/* Json input & output. */
(
close(window(c));
c = RCtlSet(fd: s.addr, opt: (rows: 1, columns: 2));
setup(c[0], "Frequency", ControlSpec(220, 440, ExponentialWarp), 330, 0);
setup(c[1], "Amplitude", ControlSpec(-60, 0, LinearWarp, 1), -12, 1);
c[0].asDictionary;
storeJson(c, "/tmp/ctl.json");
close(window(c));
j = newJson(RCtlSet, s, "/tmp/ctl.json")
)

j = newJson(RCtlSet, s, "/home/rohan/sw/sc3-ctl/help/rctl.json")

/* Inactive */
c[0].enabled = false
enabled_(at(c, 0), true)
