## PAGES

Pages are numbered from zero and are stored implicity by all page functions.

- `P` store the current page
- `-` move to the previous page
- `+` move to the next page
- `!` copy the current page into the first unused page

For `RCtl` the parameter sequence is stored as an array, one file per
page.

    dir/rctl.json
    dir/rctl/page.json

For `RTbl` each table is stored as an array, one file per table.

    dir/rtbl.json
    dir/rtbl/page/table.json

