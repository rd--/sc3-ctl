RCtl {
	var fd; /* NetAddr */
	var <bus_id; /* Nil | Int */
	var <synth_id; /* Nil | Int */
	var <synth_param; /* Nil | String | Int */
	var <name; /* StaticText */
	var <value; /* Float */
	var <>spec; /* ControlSpec */
	var <>onEdit; /* Nil | RCtl -> () */
	var goto_min, goto_def, goto_max; /* Button */
	var goto; /* HLayout */
	var <number; /* NumberBox */
	var <slider; /* Slider */
	var <view; /* VLayout */
	var <>basicColor; /* Color */
	var <opt; /* Dictionary[(String, Object)] */

	*new {
		arg fd, bus_id, synth_id, synth_param, name = "", value = 0.0, spec = 'unipolar', opt;
		^super.new.init(fd, bus_id, synth_id, synth_param, name, value, spec.asSpec, opt);
	}

	init {
		arg z_fd, z_bus_id, z_synth_id, z_synth_param, z_name, z_value, z_spec, z_opt;
		fd = z_fd;
		bus_id = z_bus_id;
		synth_id = z_synth_id;
		synth_param = z_synth_param;
		value = z_value;
		spec = z_spec;
		opt = z_opt;
		view = nil;
		basicColor = RGui.background;
		name = RGui.label(z_name, { this.value_(spec.default) }).background_(basicColor);
		slider = Slider()
			.value_(spec.unmap(z_value))
			.orientation_('horizontal')
			.knobColor_(Color.black)
			.background_(basicColor)
			.thumbSize_(4)
			.resize_(5)
			.action_({ this.internal_(slider.value) })
		;
		slider.action.value;
		number = NumberBox()
			.font_(RGui.font)
			.action_({this.value_(number.value)})
			.value_(spec.unmap(z_value))
			.resize_(5)
			.maxHeight_(24) /* ? */
			.clipLo_(spec.minval).clipHi_(spec.maxval).scroll_step_(spec.range / 50.0)
			.background_(basicColor)
		;
		goto_min = RGui.label("-", { this.value_(spec.minval) });
		goto_def = RGui.label("/", { this.value_(spec.default) });
		goto_max = RGui.label("+", { this.value_(spec.maxval) });
		goto = RGui.hLayout(goto_min, goto_def, goto_max);
		view = RGui.vLayout(
			[name, (stretch: 2)],
			RGui.hLayout(
				if(opt["use_goto"]) { goto } {nil},
				if(opt["use_number"]) { [number, (stretch: 1)] } { nil }
			),
			if(opt["use_slider"]) { slider } {nil},
			opt["vspace"]
		)
	}

	enabled_ {
		arg x;
		this.name.enabled_(x);
		this.number.enabled_(x);
		this.slider.enabled_(x)
	}

	internal {
		^spec.unmap(value)
	}

	internal_ {
		arg n;
		this.value_(spec.map(n.clip(0.0, 1.0)))
	}

	value_ {
		arg n;
		value = n;
		this.send
	}

	/* request value from scsynth (or like) - RCtlSet handles incoming /c_set */
	update {
		if(bus_id.notNil) {
			fd.sendMsg('c_get', bus_id)
		}
	}

	send {
		if(onEdit.notNil) { onEdit.value(this) } ;
		if(bus_id.notNil) { fd.sendMsg("/c_set", bus_id, value) };
		if(synth_id.notNil) { fd.sendMsg("/n_set", synth_id, synth_param, value) };
		{ this.updateView }.defer
	}

	asUGenInput {
		^bus_id.asBus
	}

	setup {
		arg z_name, z_spec, z_value, z_bus_id, z_synth_id, z_synth_param, z_onEdit;
		name.string = z_name.asString;
		spec = z_spec.asSpec;
		if(z_value.notNil) { spec.default_(z_value) };
		number.clipLo_(spec.minval).clipHi_(spec.maxval).scroll_step_(spec.range / 50.0);
		bus_id = z_bus_id;
		synth_id = z_synth_id;
		synth_param = z_synth_param;
		if(z_onEdit.notNil) { this.onEdit_(z_onEdit) };
		this.value_(z_value ? spec.default) /* +send */
	}

	asDictionary {
		^Dictionary[
			"bus_id" -> bus_id,
			"synth_id" -> synth_id,
			"synth_param" -> synth_param,
			"name" -> name.string.asString,
			"value" -> value.asFloat,
			"minval" -> spec.minval.asFloat,
			"maxval" -> spec.maxval.asFloat,
			"defval" -> spec.default.asFloat,
			"warp" -> spec.warp.asSpecifier.asString,
			"step" -> spec.step.asFloat
		]
	}

	writeJson {
		arg f;
		RJson.writeDictionary(f, this.asDictionary)
	}

	setupDictionary {
		arg d;
		var n = d.at("name") ? "";
		var v = (d.at("value") ? 0).asFloat;
		var l = (d.at("minval") ? 0).asFloat;
		var u = (d.at("maxval") ? 1).asFloat;
		var k = (d.at("defval") ? l).asFloat;
		var w = (d.at("warp") ? "linear").asSymbol;
		var s = (d.at("step") ? 0).asFloat;
		var cs = ControlSpec(l, u, w, s, k);
		bus_id = (d.at("bus_id") ? 0).asInteger;
		this.setup(n, cs, v)
	}

	updateView {
		if(view.notNil) {
			name.background_(basicColor);
			number.value_(value);
			number.background_(basicColor);
			slider.value_(this.internal)
		}
	}

}
