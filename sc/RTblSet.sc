RTblSet {
	var <tbl; /* [RTbl] */
	var dir; /* StaticText */
	var page; /* StaticText */
	var window; /* Window */
	var opt; /* Dictionary[(String, Object)] */

	*defaultOpt {
		^Dictionary[
			"numFrames" -> 256,
			"bufzero" -> 0,
			"alloc" -> true,
			"rows" -> 8,
			"columns" -> 4,
			"columnOrder" -> false,
			"rowHeight" -> 100,
			"columnWidth" -> 400,
			"resizable" -> false,
			"dir" -> "/tmp",
			"page" -> 0
		]
	}

	*new {
		arg server, opt = ();
		^super.new.init(server, opt)
	}

	init {
		arg server, z_opt;
		var o, n, v, order_f, v_s, g, m, re_order, w, h, x0, y0;
		opt = R.mergeDictLeft(z_opt, RTblSet.defaultOpt);
		w = opt["columns"] * opt["columnWidth"];
		h = opt["rows"] * opt["rowHeight"];
		x0 = 200;
		y0 = Window.screenBounds.height - 200;
		n = opt["rows"] * opt["columns"];
		tbl = (0 .. n - 1).collect({
			arg i;
			RTbl.new(server, opt["bufzero"] + i, opt["numFrames"], opt["alloc"]);
		});
		v = tbl.collect({arg i; i.view});
		dir = RGui.label(opt["dir"]);
		page = RGui.label(opt["page"]);
		m = RGui.hLayout(
			RGui.button("Z", {this.zero}),
			RGui.button("L", {this.loadPage}),
			RGui.button("P", {this.storePage}),
			page,
			RGui.button("-", {this.repage(-1)}),
			RGui.button("+", {this.repage(1)}),
			RGui.button("!", {this.duplicatePage}),
			RGui.button("D", {this.dirDialog}),
			[dir, \stretch:3]
		);
		order_f = {
			arg l;
			if(opt["columnOrder"]) {
				l.clump(opt["rows"]).flop
			} {
				l.clump(opt["columns"])
			}
		};
		v_s = order_f.(v);
		g = RGui.gridLayout(*v_s);
		window = Window("RTblSet", bounds: Rect(x0, y0, w, h), resizable: opt["resizable"])
		.layout_(RGui.vLayout(m, g))
		.background_(RGui.background);
		window.front
	}

	names_ {
		arg names;
		names.do { arg nm, i; tbl[i].name_(nm) }
	}

	at {
		arg i;
		^tbl.at(i)
	}

	operate {
		arg indices, f;
		var k = if(indices.isNil) { (0 .. tbl.size - 1) } { indices };
		k.do { arg i; f.value(tbl[i]) }
	}

	zero {
		arg indices = nil;
		this.operate(indices) { arg t; t.zero }
	}

	read {
		arg indices = nil;
		this.operate(indices) { arg t; t.read }
	}

	dir_ {
		arg nm;
		opt.put("dir", nm);
		dir.string_(nm)
	}

	dirDialog {
		FileDialog(
			{ arg fn; this.dir_(fn[0]) },
			{ },
			fileMode: 2,/* 2 = directory */
			acceptMode: 0 /* 0 = open */
		)
	}

	pageDir {
		arg n = page.string.asInteger;
		^opt["dir"] +/+ n.asString
	}

	pageExists {
		arg n;
		^PathName(this.pageDir(n)).isFolder
	}

	jsonfile {
		^opt["dir"] +/+ "rtbl.json"
	}

	/* zero all tables if directory doesn't exist */
	loadPage {
		arg indices = nil;
		var nm = this.pageDir;
		RGui.trace("RTbl.loadPage", nm);
		if(PathName(nm).isFolder) {
			this.operate(indices) { arg t; t.loadDir(nm) }
		} {
			this.zero
		}
	}

	/* makes directory if required */
	storePage {
		arg indices = nil;
		var nm = this.pageDir;
		RGui.trace("RTbl.storePage", nm);
		if(PathName(nm).isFolder.not) { nm.mkdir };
		this.operate(indices) { arg t; t.storeDir(nm) }
	}

	/* store current page,load next page */
	repage {
		arg n = 1;
		var p;
		this.storePage;
		p = max(0, page.string.asInteger + n);
		page.string_(p.asString);
		this.loadPage
	}

	duplicatePage {
		arg n = 0;
		RGui.trace("RTblSet.duplicatePage", n);
		if(this.pageExists(n).not) {
			page.string_(n.asString);
			this.storePage
		} {
			this.duplicatePage(n + 1)
		}
	}

	asDictionary {
		var a = this.tbl.collect({ arg t; t.asDictionary }).asArray;
		var d = Dictionary.new(opt.size + 1);
		d.put("tbl", a);
		d.put("opt", opt);
		^d
	}

	storeJson {
		arg path = opt["dir"] +/+ "rtbl.json";
		var f = File.open(path, "w");
		RGui.trace("RTbl.storeJson", path);
		RJson.writeObject(f, this.asDictionary);
		f.close
	}

	applyJson {
		arg a;
		a.do {
			arg d, i;
			if(i < tbl.size) {
				tbl.at(i).setupDictionary(d)
			}
		}
	}

	loadJson {
		arg nm;
		this.applyJson(nm.parseYAMLFile.at("tbl"))
	}

	*newJson {
		arg s, nm, sd = nil;
		var d = nm.parseYAMLFile;
		var a = d.at("tbl");
		var o = d.at("opt");
		var t;
		if(sd.notNil) { o.put("dir", sd) }; /* over-write any indicated directory... */
		t = this.new(server: s, opt: o.collect { arg k; RJson.interpret(k) });
		t.applyJson(a);
		{ t.loadPage }.defer(0.25); /* the buffers take some little time to allocate... */
		^t
	}

	/* this is the 'standard' setup, Json file at known location,pages likewise */
	*std {
		arg s, d;
		^this.newJson(s, d +/+ "rtbl.json", d +/+ "rtbl")
	}

}
