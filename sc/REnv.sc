REnv {
	var server; /* Server */
	var <view; /* EnvelopeView */
	var <buf; /* Buffer */
	var <>onEdit; /* Nil | REnv -> () */
	var <env; /* Env */

	*new {
		arg parent, server, bufnum, numFrames;
		^super.new.init(parent, server, bufnum, numFrames)
	}

	init {
		arg parent, server_, bufnum, numFrames;
		server = server_;
		buf = Buffer.alloc(server, numFrames, 1, nil, bufnum);
		env = Env.new([0, 0], [1], [0]);
		view = (
			EnvelopeView(parent, parent.view.bounds)
			.drawLines_(true)
			.selectionColor_(Color.red)
			.drawRects_(true)
			.resize_(5)
			.step_(0.05)
			.keepHorizontalOrder_(true)
			.mouseUpAction_({
				this.query;
				this.write;
				if(onEdit.notNil) { onEdit.value(this) }
			})
			.keyDownAction_({
				arg v, c, m;
				switch (c,
					$w, { this.write }
				)
			})
			.thumbSize_(5)
			.setEnv(env)
		)
	}

	env_ {
		arg e;
		env = e;
		view.setEnv(e)
	}

	query {
		var t, l, c;
		#t, l = view.value;
		c = env.curves; /* view.curves */
		env = Env.new(l, t.differentiate.drop(1), c);
		^env
	}

	write {
		^buf.setn(0, env.discretize(buf.numFrames))
	}

	rand4 {
		var t = [rrand(0.0, 0.5), rrand(0.5, 1.0), 1.0].differentiate;
		var l = { 1.0.rand } ! 4;
		var c = { 8.0.rand2 } ! 4;
		this.env_(Env.new(l, t, c))
	}

}
