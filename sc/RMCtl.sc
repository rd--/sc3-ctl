/* initial pass... */

RMCtl {

	var server; /* Server | NetAddr*/
	var <nctl;
	var ctl_h = 50; /* control height (pixels) */
	var ctl_w = 4; /* control width (pixels) */
	var ctl_g = 4; /* control separation (pixels) */
	var <>view; /* VLayout */

	*new {
		arg server, label_txt, nid_seq, param_seq, cspec_seq, on_edit;
		^super.new.init(server, label_txt, nid_seq, param_seq, cspec_seq, on_edit)
	}

	width {
		^(nctl * (ctl_w + ctl_g))
	}

	init {
		arg z_server, label_txt, nid_seq, param_seq, cspec_seq, on_edit;
		var l, m;
		server = z_server;
		nctl = nid_seq.size;
		l = RGui.label(label_txt);
		m = MultiSliderView(bounds: Rect(0, 0, this.width, ctl_h));
		m.value_(Array.fill(nctl, { 0.01 }));
		m.isFilled_(true);
		m.indexThumbSize_(ctl_w);
		m.gap_(ctl_g);
		m.resize_(1);
		m.action_({
			arg v;
			var ix = v.index;
			var nid = nid_seq[ix];
			var param = param_seq[ix];
			var data = cspec_seq[ix].map(v.value[ix]);
			server.sendMsg("/n_set", nid, param, data);
			if(on_edit.notNil) { on_edit.(nid, param, data) };
		});
		view = RGui.vLayout(l, m);
	}
}
