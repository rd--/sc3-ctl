RTbl {
	var <buf; /* Buffer */
	var <env; /* MultiSliderView */
	var name; /* StaticText */
	var message; /* StaticText */
	var <spec; /* ControlSpec */
	var <view; /* VLayout */
	var <>onEdit; /* Nil | RTbl -> () */

	*new {
		arg server, bufnum, numFrames, alloc=true, spec='unipolar';
		^super.new.init(server, bufnum, numFrames, alloc, spec)
	}

	init {
		arg server, bufnum, numFrames, alloc, z_spec;
		var meta;
		buf = if(alloc) {
			Buffer.alloc(server, numFrames, 1, nil, bufnum)
		} {
			Buffer.new(server, numFrames, 1, bufnum)
		};
		spec = z_spec.asSpec;
		env = (
			MultiSliderView()
			.drawLines_(true)
			.drawRects_(false)
			.isFilled_(true)
			.fillColor_(Color.grey(0.25))
			.mouseUpAction_({
				this.write;
				if(this.onEdit.notNil, {this.onEdit.value(this)});
			})
			.keyDownAction_({
				arg v, c, m;
				switch (c,
					$r, {this.read},
					$w, {this.write}
				);
			})
			.elasticMode_(1)
			.thumbSize_(1)
			.gap_(0)
			.value_(spec.unmap(spec.default) ! numFrames)
			.minHeight_(48)
			.reference_(spec.unmap(spec.default) ! numFrames)
		);
		name = RGui.label("");
		message = RGui.label("");
		meta = RGui.hLayout(
			[RGui.button("Z", { this.zero }), (align:'left')],
			RGui.button("L", { this.loadDialog }),
			RGui.button("A", { this.sf1RMSDialog }),
			RGui.button("N", { this.normalize }),
			RGui.button("S", { this.message_(RGui.specPP(spec)) }),
			4,
			RGui.hLayout(
				RGui.label("#" ++ buf.bufnum.asString).minWidth_(30).maxWidth_(30),
				[name, (stretch: 3)]),
			message
		);
		view = RGui.vLayout(meta, env)
	}

	/* Array[Float] */
	value {
		^env.value.collect { arg k; spec.map(k) }
	}

	value_ {
		arg arr;
		env.value_(arr.collect { arg k; spec.unmap(k) });
		this.write
	}

	zero {
		this.value_(spec.default ! buf.numFrames)
	}

	normalize {
		this.value_(this.value.normalize(spec.minval, spec.maxval))
	}

	sf1RMS {
		arg nm;
		var arr = R.read_sf_1(nm);
		var red = R.resample_rms(arr, buf.numFrames);
		this.value_(red)
	}

	sf1RMSDialog {
		Dialog.openPanel { arg nm; this.sf1RMS(nm) }
	}

	message_ {
		arg text;
		message.string_(text);
		{ message.string_("") }.defer(2)
	}

	spec_ {
		arg s;
		spec = s.asSpec;
		env.reference_(spec.unmap(spec.default) ! buf.numFrames);
		this.value_(this.value)
	}

	read {
		buf.getToFloatArray(0, buf.numFrames, 0.05, 3, {
			arg d;
			{ this.value_(d) }.defer
		})
	}

	write {
		^buf.setn(0, this.value)
	}

	name_ {
		arg nm;
		name.string_(nm)
	}

	storeDir {
		arg dir;
		var nm = dir +/+ buf.bufnum.asString ++ ".json";
		RJson.storeObject(nm, this.value)
	}

	loadFile {
		arg nm;
		if(PathName(nm).isFile) {
			buf.read(nm, action: { this.read })
		};
	}

	loadDir {
		arg dir;
		var nm = dir +/+ buf.bufnum.asString ++ ".json";
		var d = nm.load;
		RGui.trace("RTbl.loadDir", nm);
		this.value_(d);
		this.write
	}

	loadDialog {
		Dialog.openPanel { arg nm; this.loadFile(nm) }
	}

	/* reset bufnum */
	reindex {
		arg bufnum;
		var b = Buffer.new(buf.server, buf.numFrames, 1, bufnum);
		buf = b;
		this.read
	}

	asDictionary {
		^Dictionary[
			"name" -> name.string.asString,
			"bufnum" -> buf.bufnum.asInteger,
			"minval" -> spec.minval.asFloat,
			"maxval" -> spec.maxval.asFloat,
			"warp" -> spec.warp.asSpecifier.asString
		]
	}

	writeJson {
		arg f;
		RJson.writeDictionary(f, this.asDictionary)
	}

	setupDictionary {
		arg d;
		var n = d["name"] ? "";
		var b = (d["bufnum"] ? 0).asInteger;
		var l = (d["minval"] ? 0).asFloat;
		var u = (d["maxval"] ? 1).asFloat;
		var w = (d["warp"] ? "linear").asSymbol;
		var cs = ControlSpec(l, u, w, 0, 0);
		this.reindex(b);
		this.spec_(cs);
		this.name_(n)
	}
}
