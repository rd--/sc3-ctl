REnvSet {
	var <env; /* [REnv] */
	var window; /* Window */
	var opt; /* Dictionary[(String, Object)] */

	*defaultOpt {
		^Dictionary[
			"numFrames" -> 256,
			"bufnum" -> 0,
			"rows" -> 8,
			"columns" -> 4,
			"columnOrder" -> false,
			"rowHeight" -> 100,
			"columnWidth" -> 400,
			"resizable" -> false,
			"dir" -> "/tmp",
			"page" -> 1
		]
	}

	*new {
		arg server, opt = ();
		^super.new.init(server, opt)
	}

	init {
		arg server, z_opt;
		var grid;
		window = Window("REnvSet");
		opt = R.mergeDictLeft(z_opt, REnvSet.defaultOpt);
		env = (0 .. opt["rows"] * opt["columns"] - 1).collect {
			arg i;
			REnv(window, server, opt["bufnum"] + i, opt["numFrames"])
		};
		grid = env.collect({ arg i; i.view }).clump(opt["columns"]);
		window.layout = GridLayout.rows(*grid).spacing_(0).margins_(0);
		window.front
	}

}
