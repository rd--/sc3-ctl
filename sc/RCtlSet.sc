RCtlSet {
	var <ctl; /* Array[RCtl] */
	var <window; /* Window */
	var <dir; /* Static Text */
	var <page; /* Static Text */
	var <opt; /* Dictionary[(String, Object)] */

	*defaultOpt {
		^Dictionary[
			"rows" -> 2,
			"columns" -> 8,
			"column_order" -> false,
			"row_height" -> 50,
			"column_width" -> 90,
			"resizable" -> false,
			"dir" -> "/tmp",
			"use_number" -> true,
			"use_slider" -> true,
			"use_goto" -> false,
			"vspace" -> 2,
			"cc_latch" -> nil
		]
	}

	*new {
		arg fd, opt = ();
		^super.new.init(fd, opt);
	}

	init {
		arg fd, z_opt;
		var w, h, x0, y0, o, order_f, ctl_o, m, g;
		opt = R.mergeDictLeft(z_opt, RCtlSet.defaultOpt);
		w = opt["columns"] * opt["column_width"];
		h = 24 + (opt["rows"] * (opt["row_height"] + if(opt["use_slider"], { 10 }, { 0 })));
		x0 = 200;
		y0 = Window.screenBounds.height - 200;
		ctl = Array.fill(opt["rows"] * opt["columns"], { RCtl(fd, opt: opt) });
		OSCFunc({
			arg msg, time_, addr_, port_;
			var ix = msg[1];
			var n = msg[2];
			ctl[ix].value = n;
			}, "/c_set", fd);
		if(MIDIClient.initialized) {
			MIDIIn.control = {
				arg source, channel, number, value;
				var ix = (channel * 128) + number;
				if(ix >= 0 && { ix <= ctl.size }) {
					var cc_latch = opt["cc_latch"];
					if(cc_latch.isNil) {
						ctl[ix].internal = value / 127.0
					} {
						var prv = ctl[ix].internal;
						var nxt = value / 127.0;
						if((nxt - prv).abs < cc_latch) {
							ctl[ix].internal = nxt
						}
					}
				}
			}
		};
		order_f = {
			arg l;
			if(opt["column_order"]) {
				l.clump(opt["rows"]).flop
			} {
				l.clump(opt["columns"])
			}
		};
		ctl_o = order_f.value(ctl.collect { arg c; c.view });
		dir = RGui.label(opt["dir"]);
		page = RGui.label(opt["page"]);
		m = RGui.hLayout(
			RGui.button("U", {this.update}),
			RGui.button("P", {this.storePage}),
			page,
			RGui.button("-", {this.repage(-1)}),
			RGui.button("+", {this.repage(1)}),
			RGui.button("!", {this.duplicatePage}),
			[dir, ('stretch': 3)]
		);
		g = GridLayout.rows(*ctl_o)
		.spacing_(0)
		.margins_(0);
		window = Window("RCtl", bounds: Rect(x0, y0, w, h), resizable: opt["resizable"])
		.layout_(RGui.vLayout(m, g))
		.background_(RGui.background)
		.front;
		^this;
	}

	at {
		arg i;
		^ctl.at(i);
	}

	copySeries {
		arg first, second, last;
		^ctl.copySeries(first, second, last);
	}

	snapshot {
		arg left, right;
		^(left..right).collect({
			arg i;
			[i, this.at(i).value];
		});
	}

	restore {
		arg snapshot;
		snapshot.do({
			arg slot;
			this.at(slot.at(0)).value = slot.at(1);
		});
	}

	update {
		arg left = 0, right = ctl.size - 1;
		RGui.trace("RCtlSet.update", left, right);
		(left..right).do({
			arg n;
			this.at(n).update;
		});
	}

	pageFile {
		arg n = page.string.asInteger;
		^opt["dir"] +/+ "rctl" +/+ n.asString ++ ".json";
	}

	pageExists {
		arg n;
		^PathName(this.pageFile(n)).isFile;
	}

	storePage {
		var fn = this.pageFile;
		var f = File.open(fn, "w");
		var a = ctl.collect(_.value);
		RGui.trace("RCtlSet.storePage", fn);
		RJson.writeArray(f, a);
		f.close;
	}

	loadPage {
		var nm = this.pageFile;
		var v = if(PathName(nm).isFile) { nm.load } { [] };
		RGui.trace("RCtlSet.loadPage", nm, v);
		v.do({arg c, n; ctl[n].value_(c)});
	}

	repage {
		arg n = 1;
		var p;
		this.storePage;
		p = max(0, page.string.asInteger + n);
		page.string_(p.asString);
		this.loadPage;
	}

	duplicatePage {
		arg n = 0;
		RGui.trace("RCtlSet.duplicatePage", n);
		if(this.pageExists(n).not) {
			page.string_(n.asString);
			this.storePage
		} {
			this.duplicatePage(n + 1)
		}
	}

	asDictionary {
		var a = ctl.collect({arg c; c.asDictionary}).asArray;
		var d = Dictionary.new(opt.size + 1);
		d.put("ctl", a);
		d.put("opt", opt);
		^d;
	}

	storeJson {
		arg nm = opt["dir"] +/+ "rctl.json";
		var f;
		RGui.trace("RCtlSet.storeJson", nm);
		if(PathName(opt["dir"]).isFolder.not) {
			opt["dir"].mkdir
		};
		f = File.open(nm, "w");
		RJson.writeObject(f, this.asDictionary);
		f.close;
	}

	applyJson {
		arg a;
		a.do {
			arg d, i;
			if(i < ctl.size) {
				ctl.at(i).setupDictionary(d)
			}
		}
	}

	loadJson {
		arg nm;
		this.applyJson(nm.parseYAMLFile.at("ctl"));
	}

	*newJson {
		arg z_fd, nm;
		var d = nm.parseYAMLFile;
		var a = d.at("ctl");
		var o = d.at("opt");
		var c = this.new(fd: z_fd, opt: o.collect({arg k; RJson.interpret(k)}));
		c.applyJson(a);
		c.loadPage;
		^c
	}

	/* this is the 'standard' setup, Json file at known location, pages likewise */
	*std {
		arg s, d;
		^this.newJson(s, d +/+ "rctl.json");
	}
}
