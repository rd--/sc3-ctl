RGui {

	classvar <>font_name = "Monospace"; /* "Sans Serif" "Courier" "Helvetica" "Times" */
	classvar <>font_size = 16;

	*font {
		^Font(this.font_name, this.font_size)
	}

	*background {
		^Color.grey(0.6)
	}

	*hLayout {
		arg ...e;
		^HLayout(*e).spacing_(0).margins_(0)
	}

	*vLayout {
		arg ...e;
		^VLayout(*e).spacing_(0).margins_(0)
	}

	*gridLayout {
		arg ...e;
		^GridLayout.rows(*e).spacing_(0).margins_(0)
	}

	*label {
		arg txt, act = nil;
		^(
			StaticText()
			.string_(txt.asString)
			.font_(RGui.font)
			.maxHeight_(24)
			.mouseDownAction_(act)
		)
	}

	*integer {
		arg n, act;
		^(
			NumberBox()
			.value_(n)
			.font_(RGui.font)
			.maxHeight_(24)
			.maxWidth_(24)
			.decimals_(0)
			.step_(1)
			.action_(act)
		)
	}

	*button {
		arg nm, act;
		^(
			Button()
			.states_([[nm, Color.black, RGui.background]])
			.action_(act)
			.value_(0)
			.maxWidth_(12 * nm.size)
			.maxHeight_(24)
			.resize_(5)
			.font_(RGui.font)
			.canFocus_(false)
		)
	}

	*buttonSetText {
		arg b, txt;
		b.states_([[txt, Color.black, RGui.background]])
	}

	*specPP {
		arg spec;
		^"(%>%>%:%)".format(
			spec.minval,
			spec.default,
			spec.maxval,
			spec.warp.asSpecifier
		)
	}

	*midiInit {
		R.midiConnectAll
	}

	*trace {
		arg ...m;
		"RGui.trace: ".post;
		m.postln
	}

}
