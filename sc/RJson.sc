RJson {

	classvar precision = 4;

	*interpret {
		arg s;
		var c = s[0];
		var r = (case
			{ c.isDecDigit } { s.interpret }
			{ s == "false" } { false }
			{ s == "true" } { true }
			{ s == "null" } { nil }
			{ true } { s }
			{ ("RJson.interpret: "++ s).error }
		);
		^r
	}

	*writeNumber {
		arg f, n;
		var s = if(n.isInteger) { n.asString } { n.asStringPrec(precision) };
		f.write(s)
	}

	*writeString {
		arg f, s;
		f.write(s.asCompileString)
	}

	*writeDictionary {
		arg f, d;
		var wr = {
			arg k, v, n;
			f.write("\"");
			f.write(k);
			f.write("\": ");
			RJson.writeObject(f, v);
			if(n < (d.size - 1)) { f.write(", ") };
		};
		f.write("{");
		d.keysValuesDo(wr);
		f.write("}");
	}

	*writeEvent {
		arg f, e;
		var d = Dictionary.new;
		e.keysValuesDo { arg k, v; d.put(k.asString, v) };
		writeDictionary(f, d)
	}

	*writeArray {
		arg f, a;
		var wr = {
			arg o, n;
			RJson.writeObject(f, o);
			if(n < (a.size - 1)) { f.write("\n, ") };
		};
		f.write("[");
		a.do(wr);
		f.write("]\n");
	}

	*writeObject {
		arg f, o;
		(switch (o.class)
			{ True } { f.write("true") }
			{ False } { f.write("false") }
			{ Integer } { RJson.writeNumber(f, o) }
			{ Float } { RJson.writeNumber(f, o) }
			{ String } { RJson.writeString(f, o) }
			{ Array } { RJson.writeArray(f, o) }
			{ Dictionary } { RJson.writeDictionary(f, o) }
			{ Event } { RJson.writeEvent(f, o) }
			{ ("RJson.writeObject: " ++ o.class.asString).error }
		)
	}

	*storeObject {
		arg nm, o;
		var f;
		RGui.trace("RJson.storeObject", nm);
		f = File.open(nm, "w");
		RJson.writeObject(f, o);
		f.close;
	}
}
