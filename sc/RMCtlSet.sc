RMCtlSet {

	var <mctl; /* [RMCtl]*/
	var <window; /* Window */

	*new {
		arg server, elem, ncol;
		^super.new.init(server, elem, ncol)
	}

	width {
		^mctl.collect({ arg m; m.width }).sum + (mctl.size * 6)
	}

	init {
		arg server, elem, ncol;
		var w_rect, c_grid, nrow;
		mctl = elem.collect { arg z; RMCtl(server, z[0], z[1], z[2], z[3], z[4]) };
		nrow = mctl.size / ncol;
		w_rect = Rect(200, Window.screenBounds.height - 200, this.width / nrow, 80 * nrow);
		c_grid = mctl.collect({ arg c; c.view }).clump(ncol);
		window = Window(bounds:w_rect, resizable:false).layout_(RGui.gridLayout(*c_grid));
		window.front;
		^this
	}
}
