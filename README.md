sc3-ctl
-------

simple [sc3](http://audiosynth.com/) controllers

- md/[overview.md](?t=sc3-ctl&e=md/overview.md)
- help/[rctl.scd](?t=sc3-ctl&e=help/rctl.scd)
- help/[rmctl.scd](?t=sc3-ctl&e=help/rmctl.scd)
- help/[rtbl.scd](?t=sc3-ctl&e=help/rtbl.scd)
- help/[server.scd](?t=sc3-ctl&e=help/server.scd)

© [rd](http://rd.slavepianos.org/), 2004-2022, [gpl](http://gnu.org/copyleft/)

initial announcement:
[[2004-12-14/sc-users](?t=sc3-ctl&e=md/announce.text),
 [bham](https://www.listarc.bham.ac.uk/lists/sc-users-2004/msg07888.html)]

<!-- [gmane](http://article.gmane.org/gmane.comp.audio.supercollider.user/7334) -->
